** Erlang Cowboy Samples **

---

### What Is It? ###

Erlang code samples to be used with Cowboy or independently.

---

### How do I get set up? ###

Have a look at the articles on [Medium](https://jeerovan.medium.com)

---

### Questions ###

Send an e-mail to [tojeevansingh@gmail.com](mailto:tojeevansingh@gmail.com) for any queries.
