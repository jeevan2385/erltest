PROJECT = erltest
PROJECT_DESCRIPTION = New project
PROJECT_VERSION = 0.1.0

DEPS = cowboy jsx bbmustache gproc msgpack gun hashids
dep_cowboy_commit = 2.6.0
dep_hashids = git https://github.com/snaiper80/hashids-erlang.git 1.0.5
dep_bbmustache = git https://github.com/soranoba/bbmustache.git master
dep_msgpack = git https://github.com/msgpack/msgpack-erlang.git master

DEP_PLUGINS = cowboy

include erlang.mk
