%---- Cowboy Gun Websocket Implementation Without Behaviours ----

-module(load_gun).
-export([connection/1]).

%---- Start as Pid = spawn(load_gun,connection,[#{host => Host,port => Port,path => Path}]) -----
%-----         Pid ! start. -------
connection(State) ->
  receive
    start ->
      #{host := Host,port := Port} = State,
      %------- Connect To Host ----------
      {ok,WPID} = gun:open(Host,Port),
      connection(State#{wpid => WPID});
    {gun_up,WPID,_Proto} ->
      #{path := Path} = State,
      %------- Upgrade Connection To WebSocket -------
      gun:ws_upgrade(WPID,Path,[],#{compress => true}),
      connection(State);
    {gun_down,_WPID,ws,closed,_,_} ->
      %------ Connection Closed ------
      connection(State);
    {gun_ws_upgrade,WPID,ok,_Data} ->
      %gun:ws_send(WPID,{binary,<<"Hello World!">>}),
      %------ Connection Upgraded to websocket ---
      connection(State#{socket => WPID});
    {gun_response, WPID, _Ref, _Code, _HttpStatusCode, _Headers} -> %-- Code : fin/nofin
      %------ Error Response While Upgrade To WebSocket -------
      gun:flush(WPID),
      connection(State);
    %------ Error In Child Connection Process ----
    {gun_error, WPID, _Ref, _Reason} ->
      gun:flush(WPID),
      connection(State);
    %------ Child Connection Process Went Down -------
    {'DOWN',_PID,process,WPID,_Reason} ->
      gun:flush(WPID),
      gun:shutdown(WPID),
      connection(State);
    %------- Frame Received On WebSocket ------
    {gun_ws, WPID, Frame} ->
      case Frame of
        close ->
          self() ! stop;
        {close,_Code,_Message} ->
          self() ! stop;
        {binary,BinData} ->
          gun:ws_send(WPID,{binary,BinData});
        _ ->
          io:format("Received Unhandled Frame: ~p~n",[Frame])
      end,
      connection(State);
    %-------- GUN EVENTS DONE ------
    %-------- Events From Balancer ------
    stop ->
      #{wpid := WPID} = State,
      gun:flush(WPID),
      gun:shutdown(WPID);
    Message ->
      io:format("Received Unknown Message on Gun: ~p~n",[Message]),
      connection(State)
  end.
