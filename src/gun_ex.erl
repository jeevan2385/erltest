%---- Cowboy Gun Websocket Implementation Without Behaviours ----

-module(gun_ex).
-compile([export_all,nowarn_export_all]).

%---- Start as Pid = spawn(gun_ex,connection,[#{host => Host,port => Port,path => Path}]) -----
%-----         Pid ! start. -------
connection(State) ->
  receive
    start ->
      #{host := Host,port := Port} = State,
      %------- Connect To Host ----------
      {ok,WPID} = gun:open(Host,Port),
      io:format("Opening Gun: ~p~n",[WPID]),
      connection(State#{wpid => WPID});
    {gun_up,WPID,_Proto} ->
      #{path := Path} = State,
      io:format("Gun Up: ~p~n",[WPID]),
      %------- Upgrade Connection To WebSocket -------
      gun:ws_upgrade(WPID,Path,[],#{compress => true}),
      io:format("Upgrading.~n",[]),
      connection(State);
    {gun_down,WPID,ws,closed,_,_} ->
      %------ Connection Closed ------
      io:format("Gun Down:~p~n",[WPID]),
      connection(State);
    {gun_ws_upgrade,WPID,ok,_Data} ->
      %------ Connection Upgraded to websocket ---
      io:format("Upgraded:~p~n",[WPID]),
      connection(State#{socket => WPID});
    {gun_response, WPID, _Ref, Code, HttpStatusCode, Headers} -> %-- Code : fin/nofin
      %------ Error Response While Upgrade To WebSocket -------
      io:format("Response:~p|~p|~p~n",[Code,HttpStatusCode,Headers]),
      gun:flush(WPID),
      connection(State);
    %------ Error In Child Connection Process ----
    {gun_error, WPID, Ref, Reason} ->
      gun:flush(WPID),
      io:format("Gun Error :~p|~p|~p~n",[WPID,Ref, Reason]),
      connection(State);
    %------ Child Connection Process Went Down -------
    {'DOWN',PID,process,WPID,Reason} ->
      io:format("Gun DOWN:~p|~p|~p~n",[PID,WPID,Reason]),
      gun:flush(WPID),
      gun:shutdown(WPID),
      connection(State);
    %------- Frame Received On WebSocket ------
    {gun_ws, _WPID, Frame} ->
      case Frame of
        close ->
          self() ! stop,
          io:format("Received Close Frame W/O Payload~n",[]);
        {close,Code,Message} ->
          self() ! stop,
          io:format("Received Close Frame With Payload: ~p|~p~n",[Code,Message]);
        {text,TextData} ->
          io:format("Received Text Frame: ~p~n",[TextData]);
        {binary,BinData} ->
          io:format("Received Binary Frame: ~p~n",[BinData]);
        _ ->
          io:format("Received Unhandled Frame: ~p~n",[Frame])
      end,
      connection(State);
    %-------- GUN EVENTS DONE ------
    %-------- Events From Balancer ------
    stop ->
      #{wpid := WPID} = State,
      gun:flush(WPID),
      gun:shutdown(WPID);
    Message ->
      io:format("Received Unknown Message on Gun: ~p~n",[Message]),
      connection(State)
  after 30 * 1000 ->
    Socket = maps:get(socket,State,notfound),
    case Socket of
      notfound ->
        ok;
      _ ->
        gun:ws_send(Socket,ping)
    end,
    connection(State)
  end.

