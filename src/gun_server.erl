-module(gun_server).
-behaviour(gen_server).

%% API.
-export([start_link/0]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
                wpid,
                down_type
                }).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
	gen_server:start_link({local,?MODULE},?MODULE, [], []).

%% gen_server.

init([]) ->
  gproc:reg({p,l,gun_server}),
  process_flag(trap_exit, true),
  self() ! start,
	{ok, #state{}}.

handle_call(Request, From, State) ->
  io:format("Call:R:~p|F:~p~n",[Request,From]),
	{reply, ignored, State}.

handle_cast(Msg, State) ->
  io:format("Cast:~p~n",[Msg]),
	{noreply, State}.

handle_info(start,State) ->
  Result = gun:open("localhost",filesettings:get(http_port,3000),#{connect_timeout => 5,retry_timeout => 5}),
  NewState =
    case Result of
      {ok,WPID} ->
        State#state{wpid = WPID};
      Result ->
        io:format("OpenResult:~p~n",[Result]),
        State
    end,
  {noreply,NewState};
handle_info({gun_up,WPID,Proto},State) ->
  io:format("Gun Up: ~p~n",[WPID]),
  %------- Upgrade Connection To WebSocket -------
  gun:ws_upgrade(WPID,"/ws",[],#{compress => true}),
  io:format("Upgrading:~p|~p~n",[WPID,Proto]),
  {noreply,State#state{down_type = hard}};
handle_info({gun_down,WPID,ws,closed,_,_},State) ->
  %------ Connection Closed ------
  io:format("Gun Down:~p~n",[WPID]),
  case State#state.down_type of
    hard ->
      timer:sleep(5000),
      self() ! stop,
      self() ! start;
    soft ->
      ok
  end,
  {noreply,State};
handle_info({gun_upgrade,WPID,_Ref,_Code,_Data},State) ->
  %------ Connection Upgraded to websocket ---
  io:format("Upgraded:~p~n",[WPID]),
  gun:ws_send(WPID,{text,"Hello"}),
  {noreply,State};
handle_info({gun_response, WPID, _Ref, Code, HttpStatusCode, Headers},State) -> %-- Code : fin/nofin
  %------ Error Response While Upgrade To WebSocket -------
  io:format("Response:~p|~p|~p~n",[Code,HttpStatusCode,Headers]),
  gun:flush(WPID),
  {noreply,State};
%------ Error In Child Connection Process ----
handle_info({gun_error, WPID, Ref, Reason},State) ->
  gun:flush(WPID),
  io:format("Gun Error :~p|~p|~p~n",[WPID,Ref, Reason]),
  {noreply,State};
%------ Child Connection Process Went Down -------
handle_info({'DOWN',PID,process,WPID,Reason},State) ->
  io:format("Gun DOWN:~p|~p|~p~n",[PID,WPID,Reason]),
  gun:flush(WPID),
  gun:shutdown(WPID),
  {noreply,State};
%------- Frame Received On WebSocket ------
handle_info({gun_ws, _WPID, Frame},State) ->
  NewState =
    case Frame of
      close ->
        self() ! stop,
        io:format("Received Close Frame W/O Payload~n",[]),
        State#state{down_type = soft};
      {close,Code,Message} ->
        self() ! stop,
        io:format("Received Close Frame With Payload: ~p|~p~n",[Code,Message]),
        State#state{down_type = soft};
      {text,TextData} ->
        io:format("Received Text Frame: ~p~n",[TextData]),
        State;
      {binary,BinData} ->
        io:format("Received Binary Frame: ~p~n",[BinData]),
        State;
      _ ->
        io:format("Received Unhandled Frame: ~p~n",[Frame]),
        State
    end,
  {noreply,NewState};
%------ Internal Events -------
handle_info(stop,State) ->
  case State#state.wpid of
    <<>> ->
      ok;
    Wpid ->
      gun:flush(Wpid),
      gun:shutdown(Wpid)
  end,
  {noreply,State#state{wpid = <<>>}};
handle_info(Info, State) ->
  io:format("Info:~p~n",[Info]),
	{noreply, State}.

terminate(_Reason, _State) ->
  %----- Not Relevant To Gun , Just Backing Up Settings---
  func:dump_table(filesetting,"../../settings.txt"),
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.
