%----- Cowboy websocket connection handling ----
-module(load_ws).

-export([init/2]).
-export([websocket_init/1]).
-export([websocket_handle/2]).
-export([websocket_info/2]).
-export([terminate/3]).

-record(state,{}).

init(Req, _Opts) ->
  {cowboy_websocket,Req,#state{},#{
        idle_timeout => 30000}}.

websocket_init(State) ->
  %------ To send events from shell to this process ------
  gproc:reg({p,l,wsocket}),
  {ok,State}.

websocket_handle({binary,Msg},State) ->
  {reply,{binary,Msg},State};
websocket_handle(Frame, State) ->
  io:format("Received Frame: ~p~n",[Frame]),
  {ok, State}.

websocket_info(_Info, State) ->
  {ok, State}.

terminate(_Reason,_Req,_State) ->
  ok.
