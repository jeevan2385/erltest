%----- Cowboy websocket connection handling ----
-module(ws_socket).

-export([init/2]).
-export([websocket_init/1]).
-export([websocket_handle/2]).
-export([websocket_info/2]).
-export([terminate/3]).

-record(state,{}).

init(Req, _Opts) ->
  {cowboy_websocket,Req,#state{}}.

websocket_init(State) ->
  %------ To send events from shell to this process ------
  gproc:reg({p,l,wsocket}),
  {ok,State}.

websocket_handle({_,<<>>},State) ->
  {ok,State};
websocket_handle({binary,Msg},State) ->
  {Status,Message} = msgpack:unpack(Msg,[{unpack_str,as_binary}]),
  case Status of
    ok ->
      self() ! {message,Message};
    error ->
      self() ! {error,<<"Error Occured While Unpacking">>}
  end,
  {ok,State};
websocket_handle(Frame, State) ->
  io:format("Received Frame: ~p~n",[Frame]),
  {ok, State}.

websocket_info(Info, State) ->
  case Info of
    {error,Error} ->
      {reply,{binary,response(#{error => Error})},State};
    {message,Map} ->
      {reply,{binary,response(#{message => Map})},State};
    shutdown  ->
      {stop,State};
    _ ->
      {ok, State}
  end.

terminate(_Reason,_Req,_State) ->
  ok.
%--------- Helpers ----------
response(Map) ->
  msgpack:pack(Map,[{pack_str,from_binary}]).
