-module(erltest_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
  ets:new(filesetting,[named_table,set,public,{read_concurrency,true},{write_concurrency,true}]),
  func:restore_settings("../../settings.txt"),
  Dispatch = cowboy_router:compile([
    {'_', [
           {"/", http_home, []},
           {"/static/[...]", cowboy_static,
            {priv_dir, erltest, "static",
               [{mimetypes, cow_mimetypes, all}]}
           },
           {"/favicon.ico", cowboy_static,
            {priv_file, erltest, "static/favicon.ico"}
           },
           {"/ws", ws_socket, []},
           {"/lws", load_ws, []}
          ]}
    ]),
  Port = filesettings:get(http_port,3000),
  {ok,_} = cowboy:start_clear(http,
                    [{port, Port}],
                    #{
                     compress => true,
                     env => #{dispatch => Dispatch}}),
	erltest_sup:start_link().

stop(_State) ->
	ok.
