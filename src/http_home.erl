-module(http_home).
-behavior(cowboy_handler).

-export([init/2]).

init(Req, State) ->
  Method = cowboy_req:method(Req),
  HasBody = cowboy_req:has_body(Req),
  Reply = handle(Method,HasBody,Req),
  {ok,Reply,State}.

handle(<<"GET">>,_,Req) ->
  Template = bbmustache:parse_file(<<"../../priv/home.html">>),
  HtmlBody = bbmustache:compile(Template,[]),
  cowboy_req:reply( 200,
                    #{},
                    HtmlBody,
                    Req);
handle(_,_,Req) ->
  cowboy_req:reply( 200,
                    #{},
                    <<>>,
                    Req).
