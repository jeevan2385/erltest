-module(func).
-compile([export_all,nowarn_export_all]).

load(Host,Port,Path,Number) ->
  [begin
    Pid = spawn(load_gun,connection,[#{host => Host,port => Port,path => Path}]),
    Pid ! start
   end
   || _X <- lists:seq(1,Number)].

connections() ->
  length(gproc:select([{{{p,l,wsocket},'_','$1'},[],['$1']}])).

%------ Backup/Restore Tables -------
dump_table(Table,FileName) ->
  Records = ets:tab2list(Table),
  write_terms(FileName,Records).
restore_settings(FileName) ->
  Records =
    case file:consult(FileName) of
      {error,_} ->
        [];
      {ok,Lines} ->
        Lines
    end,
  [ets:insert(filesetting,Record) || Record <- Records].
restore_table(FileName) ->
  {ok,Records} = file:consult(FileName),
  [db:write_record(Record) || Record <- Records].
write_terms(Filename, List) ->
    Format = fun(Term) -> io_lib:format("~p.~n", [Term]) end,
    Text = lists:map(Format, List),
    file:write_file(Filename, Text).
