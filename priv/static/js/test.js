var debug = false;
var demo = false;
function init(){
}
//------ Socket Implementation Starts ----------
var websocket = null;
var pinger = null;
function setPing(){
  pinger = setInterval(
            function(){
              sendJSON("");
            },30000);
}
function connectSocket(){
  //Update UI -- Show Connecting
  $(".socketcheck .typing").css("visibility","visible");
  
  //Initialize websocket
  var protocol = location.protocol;
  var webprotocol = null;
  if(protocol == 'https:') {
    webprotocol = 'wss';
  }
  else {
    webprotocol = 'ws';
  }
  var wsUri = webprotocol + '://'+location.host+'/ws';
  websocket = new WebSocket(wsUri);
  websocket.binaryType = 'arraybuffer';
  websocket.onopen = function(evt) { onOpen(evt) };
  websocket.onclose = function(evt) { onClose(evt) };
  websocket.onmessage = function(evt) { onMessage(evt) };
  websocket.onerror = function(evt) { onError(evt) };
}
function onOpen(evt) {
  log("Connected");
  $("#messages").prepend("<div>Connected</div>");
  $(".socketcheck .typing").css("visibility","hidden");
  setPing();
}
function onClose(evt) {
  log("Disconnected");
  clearInterval(pinger);
  websocket = null;
}
function onMessage(evt) {
  //update UI
  showResult(1,"");
  $("#notifications").html("");

  // handle responses
  var json = null;
  if(evt.data.byteLength === undefined){
    json = JSON.parse(evt.data);
  } else {
    json = msgpack.decode(new Uint8Array(evt.data));
  }
  $("#messages").prepend("<div>"+JSON.stringify(json)+"</data>");
}
function onError(evt) {
  log(evt);
}
function sendJSON(JSON){
  if (websocket){
     // send binary frames 
     websocket.send(msgpack.encode(JSON));
  }
}
function connect(){
  connectSocket();
}
//------ Socket Implementation Ends ----------
//------ Helper Functions ------
function showResult(status,message){
  if(status){
    $(".socketcheck .response").html(message).removeClass("red").removeClass("green").addClass("green");
  } else {
    $(".socketcheck .response").html(message).removeClass("red").removeClass("green").addClass("red");
  }
  $(".socketcheck .typing").css("visibility","hidden");
}
function log(message){
  if(debug)console.log(message);
}
function notify(Success,Message){
  var notify_class = Success ? 'alert-success' : 'alert-danger';
  var notify_icon = Success ? 'check' : 'error_outline';
  var html = '<div class="alert '+notify_class+'">'+
               '<div class="container">'+
                 '<div class="alert-icon">'+
                  '<i class="material-icons">'+notify_icon+'</i>'+
                '</div>'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                  '<span aria-hidden="true"><i class="material-icons">clear</i></span>'+
                '</button>'+
                  Message +
               '</div>'+
             '</div>';
  $("#notifications").html(html);
}
